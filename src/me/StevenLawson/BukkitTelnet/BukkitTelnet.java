package me.StevenLawson.BukkitTelnet;

import java.util.ArrayList;
import me.StevenLawson.BukkitTelnet.session.ClientSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.bukkit.Server;
import org.bukkit.plugin.java.JavaPlugin;

public class BukkitTelnet extends JavaPlugin
{
    public static BukkitTelnet plugin;
    public static Server server;

    @Override
    public void onLoad()
    {
        plugin = this;
        server = plugin.getServer();

        TelnetLogger.setPluginLogger(plugin.getLogger());
        TelnetLogger.setServerLogger(server.getLogger());
    }

    @Override
    public void onEnable()
    {
        TelnetConfig.getInstance().loadConfig();

        ((Logger) LogManager.getRootLogger()).addAppender(TelnetLogAppender.getInstance());

        TelnetServer.getInstance().startServer();

        TelnetLogger.info(plugin.getName() + " v" + plugin.getDescription().getVersion() + " enabled");

    }

    @Override
    public void onDisable()
    {
        TelnetServer.getInstance().stopServer();

        TelnetLogger.info(plugin.getName() + " disabled.");
    }
    
    public static ArrayList<ClientSession> getClientSessions()
    {
        return TelnetServer.getInstance().getSocketListener().getClientSessions();
    }
}
